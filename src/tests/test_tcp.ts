import {
    makeClient,
    makeServer
} from "../mysocket/index.js"


async function _test1(): Promise<void>
{
    const serverMsg = "Hi from Server"
    const clientMsg = "Hi from Client"
    let clientCbCalls = 0
    const expectedClientCbCalls = 3

    const server = makeServer({"socketType": "tcp-ipcs"})
    const client = makeClient({"socketType": "tcp-ipcs"})

    server.setReceiveCallbackFactory((connection) => async(data) => {
        const msg = data.buffer.toString("utf-8")
        console.log(`[Server] Data = ${msg}`)
        if (msg !== clientMsg)
        {
            const err = "[Server] Error on message"
            console.error(err)
            throw new Error(err)
        }
        await connection.send({
            "data": Buffer.from(serverMsg, "utf-8"),
        })
    })

    client.setReceiveCallback(async(data) => {
        const msg = data.buffer.toString("utf-8")
        console.log(`[Client] CB Data = ${msg}`)
        if (msg !== serverMsg)
        {
            const err = "[Client] CB Error on message"
            console.error(err)
            throw new Error(err)
        }
        clientCbCalls += 1
    })

    await server.open()
    await server.open()

    await client.open()
    await client.open()

    console.log("ROUND 1")
    const data1 = await client.sendReceive(
        Buffer.from(clientMsg, "utf-8"),
        {"timeoutMillis": 2000}
    )
    const msg1 = data1.buffer.toString("utf-8")
    console.log(`[Client] Data = ${msg1}`)
    if (msg1 !== serverMsg)
    {
        const err = "[Client] Error on message (1)"
        console.error(err)
        throw new Error(err)
    }

    console.log("ROUND 2")
    const data2 = await client.sendReceive(
        clientMsg,
        {"timeoutMillis": 2000}
    )
    const msg2 = data2.buffer.toString("utf-8")
    console.log(`[Client] Data = ${msg2}`)
    if (msg2 !== serverMsg)
    {
        const err = "[Client] Error on message (2)"
        console.error(err)
        throw new Error(err)
    }

    await server.close()
    await server.close()

    await client.close()
    await client.close()

    await server.open()
    await client.open()

    console.log("ROUND 3")
    const data3 = await client.sendReceive(
        clientMsg,
        {"timeoutMillis": 2000}
    )
    const msg3 = data3.buffer.toString("utf-8")
    console.log(`[Client] Data = ${msg3}`)
    if (msg2 !== serverMsg)
    {
        const err = "[Client] Error on message (3)"
        console.error(err)
        throw new Error(err)
    }

    await server.close()
    await client.close()

    if (clientCbCalls !== expectedClientCbCalls)
    {
        const err = "[Client] Error on expected callback calls"
        console.error(err)
        throw new Error(err)
    }

    console.log("ROUND 4: testing failure")
    await client.open()
    try
    {
        await client.sendReceive(
            clientMsg,
            {"timeoutMillis": 2000}
        )
        return Promise.reject(new Error("[Client] Expected to fail"))
    }
    catch
    {}
    await client.close()

    console.log("SUCCESS!")
}


// async function _test2(): Promise<void>
// {
//     const client = makeClientSocket({
//         "socketType": "tcp-ipcs",
//         "autoReconnectTimeout": 1000
//     })
//     await client.open()
// }


_test1().catch((err) => {
    console.error(err)
    process.exit(1)
})

// _test2().catch((err) => {
//     console.error(err)
//     process.exit(1)
// })
