import {
    ReceivedMessage,
    SentMessage
} from "../index.js";


/** Options for receive method. */
export type ServerReceiveOpts = {
    /** Timeout for waiting a reply. */
    timeoutMillis: number
}


/** Partial of ServerReceiveOpts.  */
export type PartialServerReceiveOpts = Partial<ServerReceiveOpts>


/**
 * Generic server connnection.
 */
export abstract class Connection
{
    private readonly _remote: string
    protected readonly _removeConnection: (connection: Connection) => void

    /**
     * Constructor.
     * @param remote - The remote adress.
     */
    public constructor(
        address: string,
        port: number,
        removeConnection: (connection: Connection) => void
    )
    {
        this._remote = Connection.makeRemoteKey(address, port)
        this._removeConnection = removeConnection
    }

    /**
     * Get the remote address.
     * @returns The remote address.
     */
    public get remote(): string
    {
        return this._remote
    }

    /**
     * Send a message and receive the response.
     * Combine a `send()` and a `receive()`.
     * @param msg - The message.
     * @param opts - The options.
     * @returns The received data.
     */
    public async sendReceive(
        msg: SentMessage,
        opts?: PartialServerReceiveOpts
    ): Promise<ReceivedMessage>
    {
        const self = this
        return self.send(msg)
            .then(async() => {return self.receive(opts)})
    }

    public static makeRemoteKey(address: string, port: number)
    {
        return `${address}:${String(port)}`
    }

    /**
     * Close the connection.
     * @returns A completion promise.
     */
    public abstract close(): Promise<void>

    /**
     * Receive a message.
     * If the socket is closed, fail.
     * If an operation is already in progress, fail.
     * Option defaults:
     * - opts.timeoutMillis: the maximum awaiting timeout. Zero or less, or not
     *   given, means no timeout. Default is DEFAULT_RECEIVE_TIMEOUT_MILLIS.
     * @param opts - The options.
     * @returns The received data.
     */
    public abstract receive(
        opts?: PartialServerReceiveOpts
    ): Promise<ReceivedMessage>

    /**
     * Send a message.
     * If the socket is closed, fail.
     * If an operation is already in progress, fail.
     * @param msg - The message.
     * @returns A completion promise.
     */
    public abstract send(msg: SentMessage): Promise<void>

    /**
     * Set receive callback.
     * @param receive - The callback
     */
    public abstract setReceiveCallback(
        receive: (msg: ReceivedMessage) => void
    ): void
}
