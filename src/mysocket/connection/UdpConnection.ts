import {
    Connection,
    DEFAULT_RECEIVE_TIMEOUT_MILLIS,
    PartialServerReceiveOpts,
    ReceivedMessage,
    SentMessage,
    ServerReceiveOpts
} from "../index.js";
import type dgram from "node:dgram"


export class UdpConnection
    extends Connection
{
    private readonly _address: string
    private readonly _port: number
    private _receive: (msg: ReceivedMessage) => void
    private _reject: ((err: Error) => void) | null
    private _resolveData: ((msg: ReceivedMessage) => void) | null
    private _socket: dgram.Socket | null
    private _timeoutId: NodeJS.Timeout | null

    public constructor(
        socket: dgram.Socket,
        rinfo: dgram.RemoteInfo,
        removeConnection: (connection: Connection) => void
    )
    {
        super(
            rinfo.address,
            rinfo.port,
            removeConnection
        )
        this._address = rinfo.address
        this._port = rinfo.port
        this._receive = (_msg: ReceivedMessage) => {}
        this._reject = null
        this._resolveData = null
        this._socket = socket
        this._timeoutId = null
    }

    public override close(): Promise<void>
    {
        const self = this
        if (self._socket === null) return Promise.resolve()
        self._socket = null
        self._removeConnection(self)
        return Promise.resolve()
    }

    public override receive(
        opts?: PartialServerReceiveOpts
    ): Promise<ReceivedMessage>
    {
        const self = this
        const fullOpts: ServerReceiveOpts = {
            "timeoutMillis": DEFAULT_RECEIVE_TIMEOUT_MILLIS,
            ...opts
        }
        return new Promise((
            resolve: (buf: ReceivedMessage) => void,
            reject: (err: Error) => void
        ) =>
        {
            if (self._socket === null)
            {
                reject(new Error("Socket closed"))
                return
            }
            else if (self._reject !== null)
            {
                reject(new Error("Operation already in progress"))
                return
            }
            self._resolveData = (msg) =>
            {
                if (self._timeoutId !== null) clearTimeout(self._timeoutId)
                self._timeoutId = null
                self._reject = null
                self._resolveData = null
                resolve(msg)
            }
            self._reject = (err) =>
            {
                if (self._timeoutId !== null) clearTimeout(self._timeoutId)
                self._timeoutId = null
                self._reject = null
                self._resolveData = null
                reject(err)
            }

            if (fullOpts.timeoutMillis > 0)
            {
                self._timeoutId = setTimeout(() => {
                    self._timeoutId = null
                    self._reject = null
                    self._resolveData = null
                    reject(new Error("Receive timeout"))
                }, fullOpts.timeoutMillis)
            }
        })
    }

    public override send(msg: SentMessage): Promise<void>
    {
        const self = this
        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) => {
            if (self._socket === null)
            {
                reject(new Error("Socket closed"))
                return
            }
            else if (self._reject !== null)
            {
                reject(new Error("Operation already in progress"))
                return
            }
            self._reject = reject
            self._resolveData = null
            self._socket.send(msg.data, self._port, self._address, () =>
            {
                self._reject = null
                self._resolveData = null
                resolve()
            })
        })
    }

    public override setReceiveCallback(
        receive: (msg: ReceivedMessage) => void
    ): void
    {
        this._receive = receive
    }

    public _onData(buf: Buffer): void
    {
        const self = this
        const msg: ReceivedMessage = {
            "address": self._address,
            "buffer": buf,
            "port": self._port
        }

        if (self._resolveData === null)
        {
            self._receive(msg)
            return
        }

        const resolveData = self._resolveData
        self._reject = null
        self._resolveData = null

        self._receive(msg)
        resolveData(msg)
    }
}
