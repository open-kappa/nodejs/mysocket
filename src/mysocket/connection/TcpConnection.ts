import {
    Connection,
    DEFAULT_RECEIVE_TIMEOUT_MILLIS,
    PartialServerReceiveOpts,
    ReceivedMessage,
    SentMessage,
    ServerReceiveOpts
} from "../index.js";
import {Buffer} from "node:buffer"
import net from "node:net"


/**
 * TCP server connnection.
 */
export class TcpConnection
    extends Connection
{
    private _receive: (msg: ReceivedMessage) => void
    private _reject: ((err: Error) => void) | null
    private _resolveData: ((msg: ReceivedMessage) => void) | null
    private _socket: net.Socket | null
    private _timeoutId: NodeJS.Timeout | null

    public constructor(
        socket: net.Socket,
        removeConnection: (connection: Connection) => void
    )
    {
        super(
            socket.remoteAddress ?? "",
            socket.remotePort ?? 0,
            removeConnection
        )
        this._receive = (_msg: ReceivedMessage) => {}
        this._reject = null
        this._resolveData = null
        this._socket = socket
        this._timeoutId = null

        const self = this
        socket.on("data", (buf) => {self._onData(buf)})
        socket.on("close", (hadError) => {self._onClose(hadError)})
    }

    public override close(): Promise<void>
    {
        const self = this
        if (self._socket === null) return Promise.resolve()
        const socket = self._socket
        self._socket = null
        return new Promise((
            resolve: () => void,
            _reject: (err: Error) => void
        ) =>
        {
            socket.end(() => {
                socket.destroy()
                resolve()
            })
        })
    }

    public override async receive(
        opts?: PartialServerReceiveOpts
    ): Promise<ReceivedMessage>
    {
        const self = this
        const fullOpts: ServerReceiveOpts = {
            timeoutMillis: DEFAULT_RECEIVE_TIMEOUT_MILLIS,
            ...opts
        }
        return new Promise((
            resolve: (msg: ReceivedMessage) => void,
            reject: (err: Error) => void
        ) => {
            if (self._socket === null)
            {
                reject(new Error("Socket closed"))
                return
            }
            else if (self._reject !== null)
            {
                reject(new Error("Operation already in progress"))
                return
            }
            self._resolveData = (msg) =>
            {
                if (self._timeoutId !== null) clearTimeout(self._timeoutId)
                self._timeoutId = null
                self._reject = null
                self._resolveData = null
                resolve(msg)
            }
            self._reject = (err) =>
            {
                if (self._timeoutId !== null) clearTimeout(self._timeoutId)
                self._timeoutId = null
                self._reject = null
                self._resolveData = null
                reject(err)
            }

            if (fullOpts.timeoutMillis > 0)
            {
                self._timeoutId = setTimeout(() => {
                    self._timeoutId = null
                    self._reject = null
                    self._resolveData = null
                    reject(new Error("Receive timeout"))
                }, fullOpts.timeoutMillis)
            }
        })
    }

    public override send(msg: SentMessage): Promise<void>
    {
        const self = this
        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) => {
            if (self._socket === null)
            {
                reject(new Error("Socket closed"))
                return
            }
            else if (self._reject !== null)
            {
                reject(new Error("Operation already in progress"))
                return
            }
            self._reject = reject
            self._resolveData = null
            self._socket.write(msg.data, (err?: Error) =>
            {
                self._reject = null
                self._resolveData = null
                if (typeof err !== "undefined")
                {
                    reject(err)
                    return
                }
                resolve()
            })
        })
    }

    public override setReceiveCallback(
        receive: (msg: ReceivedMessage) => void
    ): void
    {
        this._receive = receive
    }

    private _onClose(_hasError: boolean): void
    {
        const self = this
        self._socket = null
        self._removeConnection(self)
        if (self._reject === null) return
        const reject = self._reject
        self._reject = null
        self._resolveData = null
        reject(new Error("Closed on error"));
    }

    private _onData(buf: Buffer): void
    {
        const self = this
        const msg: ReceivedMessage = {
            "address": self._socket?.remoteAddress ?? "",
            "buffer": buf,
            "port": self._socket?.remotePort ?? 0
        }

        if (self._resolveData === null)
        {
            self._receive(msg)
            return
        }

        const resolveData = self._resolveData
        self._reject = null
        self._resolveData = null

        self._receive(msg)
        resolveData(msg)
    }
}
