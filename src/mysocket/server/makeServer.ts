import {
    TcpServer,
    UdpServer,
    type PartialTcpServerConfig,
    type PartialUdpServerConfig,
    type Server
} from "../index.js"


export type AnyPartialServerConfig =
PartialTcpServerConfig
    | PartialUdpServerConfig


export type RetServer<TConfig extends AnyPartialServerConfig> =
    TConfig["socketType"] extends "udp" ? UdpServer
    : TConfig["socketType"] extends "tcp-ipcs" ? TcpServer
    : Server


export function makeServer<
    TConfig extends AnyPartialServerConfig
>(
    config: TConfig
): RetServer<TConfig>
{
    switch (config.socketType)
    {
        case "tcp-ipcs": return new TcpServer(
                config as PartialTcpServerConfig
            ) as RetServer<TConfig>
        case "udp": return new UdpServer(
                config as PartialUdpServerConfig
            ) as RetServer<TConfig>
    }
}
