import {
    Server,
    type ServerConfig,
    type PartialServerConfig as PartialServerConfig,
    DEFAULT_PORT,
    DEFAULT_SERVER_ADDRESS,
    DEFAULT_AUTO_RECONNECT_TIMEOUT,
    UdpConnection
} from "../index.js"
import dgram from "node:dgram"


export type SpecificUdpServerConfig =
    dgram.SocketOptions
    & {
        socketType: "udp"
    }


export type UdpServerConfig =
    ServerConfig
    & SpecificUdpServerConfig


export type PartialUdpServerConfig =
    Partial<SpecificUdpServerConfig>
    & PartialServerConfig


export class UdpServer
    extends Server<UdpServerConfig, UdpConnection>
{
    private _server: dgram.Socket | null

    /**
     * Constructor.
     * Defaults:
     * - address: DEFAULT_SERVER_ADDRESS
     * - autoReconnectTimeout: DEFAULT_AUTO_RECONNECT_TIMEOUT
     * - port: DEFAULT_PORT
     * - socketType: "udp"
     * - type: "udp4"
     * @param config - The configuration.
     */
    public constructor(config?: PartialUdpServerConfig)
    {
        const fullConfig: UdpServerConfig = {
            "address": DEFAULT_SERVER_ADDRESS,
            "autoReconnectTimeout": DEFAULT_AUTO_RECONNECT_TIMEOUT,
            "port": DEFAULT_PORT,
            "socketType": "udp",
            "type": "udp4",
            ...config,
        }
        super(fullConfig)
        this._server = null
    }

    public override _closeSocket(): Promise<void>
    {
        const self = this
        if (self._server === null) return Promise.resolve()
        const server = self._server
        self._server = null

        return new Promise((
            resolve: () => void,
            _reject: (err: Error) => void
        ) => {
            server.close(() =>
            {
                resolve()
            })
        })
    }

    public override async open(): Promise<void>
    {
        const self = this
        if (self._server !== null) return Promise.resolve()
        const server = dgram.createSocket(self._config)
        self._server = server
        server.on("message", (msg, rinfo) =>
        {
            if (!self._hasConnection(rinfo.address, rinfo.port))
            {
                const connection = new UdpConnection(
                    server,
                    rinfo,
                    (conn) => {self._removeConnection(conn)}
                )
                if (self._receiveCallbackFactory !== null)
                {
                    const receive = self._receiveCallbackFactory(connection)
                    connection.setReceiveCallback(receive)
                }
                self._addConnection(connection)
            }
            const conn = self._getConnection(rinfo.address, rinfo.port)
            if (typeof conn === "undefined") return
            conn._onData(msg)
        })
        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) =>
        {
            const onErr = (err: Error) => {
                self.close().then(() => {reject(err)}).catch(reject)
            }
            server.once("error", onErr)
            server.bind(self._config.port, self._config.address, () =>
            {
                server.removeListener("error", onErr)
                resolve()
            })
        })
    }
}
