import {
    forAll,
    Connection,
    type ReceivedMessage,
    type SocketType
} from "../index.js"


export type ServerConfig = {
    address: string
    autoReconnectTimeout: number
    port: number
    socketType: SocketType
}


export type PartialServerConfig =
    {socketType: SocketType}
    & Partial<ServerConfig>


export type ReceiveCallbackFactory<TConnection extends Connection> =
    (connection: TConnection) => (msg: ReceivedMessage) => Promise<void>


/**
 * Base class for servers.
 * @typeparam TConfig - The sub-class configuration.
 * @typeparam TConnection - The sub-class connection.
 */
export abstract class Server<
    TConfig extends ServerConfig = ServerConfig,
    TConnection extends Connection = Connection
>
{
    protected _receiveCallbackFactory:
        ReceiveCallbackFactory<TConnection> | null

    protected readonly _config: TConfig
    private readonly _connections: Map<string, TConnection>

    /**
     * Constructor.
     * @param config - The configuration.
     */
    public constructor(config: TConfig)
    {
        this._receiveCallbackFactory = null
        this._config = config
        this._connections = new Map()
    }

    /**
     * Close the socket.
     * If already closed, do nothing.
     * @returns A completion promise.
     */
    public async close(): Promise<void>
    {
        const self = this
        const connections = [...self._connections.values()]
        self._connections.clear()
        const ret = self._closeSocket()
        await forAll(
            async(conn: TConnection): Promise<void> =>
            {
                return conn.close()
            },
            ...connections.map((c) => [c])
        )
        return ret
    }

    /**
     * Set a factory of callbacks to receive data asynchronously.
     * @param cb - The callback.
     */
    public setReceiveCallbackFactory(
        cb: ReceiveCallbackFactory<TConnection> | null
    ): void
    {
        this._receiveCallbackFactory = cb
    }

    /**
     * Open the socket.
     * If already open, do nothing.
     * If connection ongoing, wait connection.
     * @returns A completion promise.
     */
    public abstract open(): Promise<void>

    /**
     * Add a connection.
     */
    protected _addConnection(connection: TConnection): void
    {
        this._connections.set(connection.remote, connection)
    }

    /**
     * Add a connection.
     */
    protected _getConnection(
        address: string,
        port: number
    ): TConnection | undefined
    {
        return this._connections.get(Connection.makeRemoteKey(address, port))
    }

    /**
     * Add a connection.
     */
    protected _hasConnection(address: string, port: number): boolean
    {
        return this._connections.has(Connection.makeRemoteKey(address, port))
    }

    /**
     * Remove a connection.
     */
    protected _removeConnection(connection: Connection): void
    {
        this._connections.delete(connection.remote)
    }

    /**
     * Close the socket.
     * @returns A completion promise.
     */
    protected abstract _closeSocket(): Promise<void>
}
