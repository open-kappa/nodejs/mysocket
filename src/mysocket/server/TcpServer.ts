import {
    Server,
    type ServerConfig,
    type PartialServerConfig,
    DEFAULT_PORT,
    DEFAULT_SERVER_ADDRESS,
    DEFAULT_AUTO_RECONNECT_TIMEOUT,
    TcpConnection,
} from "../index.js"
import net from "node:net"


/** Options specific to TCP. */
export type SpecificTcpServerConfig =
    net.ServerOpts
    & {
        socketType: "tcp-ipcs"
    }


/** TCP server options. */
export type TcpServerConfig =
    ServerConfig
    & SpecificTcpServerConfig


/** Partial of TcpServerConfig. */
export type PartialTcpServerConfig =
    Partial<SpecificTcpServerConfig>
    & PartialServerConfig


/** TCP server. */
export class TcpServer
    extends Server<
        TcpServerConfig,
        TcpConnection
    >
{
    private _server: net.Server | null

    /**
     * Constructor.
     * Defaults:
     * - address: DEFAULT_SERVER_ADDRESS
     * - autoReconnectTimeout: DEFAULT_AUTO_RECONNECT_TIMEOUT
     * - port: DEFAULT_PORT
     * - socketType: "tcp-ipcs"
     * @param config - The configuration.
     */
    public constructor(config?: PartialTcpServerConfig)
    {
        const fullConfig: TcpServerConfig = {
            "address": DEFAULT_SERVER_ADDRESS,
            "autoReconnectTimeout": DEFAULT_AUTO_RECONNECT_TIMEOUT,
            "port": DEFAULT_PORT,
            "socketType": "tcp-ipcs",
            ...config,
        }
        super(fullConfig)
        this._server = null
    }

    protected override async _closeSocket(): Promise<void>
    {
        const self = this
        if (self._server === null) return Promise.resolve()
        const server = self._server
        self._server = null
        return new Promise<void>((
            resolve: () => void,
            reject: (err: Error) => void
        ) =>
        {
            server.close((err?: Error) =>
            {
                if (typeof err !== "undefined")
                {
                    reject(err)
                    return
                }
                resolve()
            })
        })
    }

    public override async open(): Promise<void>
    {
        const self = this
        if (self._server !== null) return Promise.resolve()
        const server = net.createServer(self._config, (socket) =>
        {
            const connection = new TcpConnection(
                socket,
                (conn) => {self._removeConnection(conn)}
            )
            if (self._receiveCallbackFactory !== null)
            {
                const receive = self._receiveCallbackFactory(connection)
                connection.setReceiveCallback(receive)
            }
            self._addConnection(connection)
        })
        self._server = server
        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) =>
        {
            const onErr = (err: Error) => {
                self.close().then(() => {reject(err)}).catch(reject)
            }
            server.once("error", onErr)
            server.listen(self._config.port, self._config.address, () =>
            {
                server.removeListener("error", onErr)
                resolve()
            })
        })
    }
}
