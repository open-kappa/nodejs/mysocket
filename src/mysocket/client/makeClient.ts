import {
    TcpClient,
    UdpClient,
    type Client,
    type PartialTcpClientConfig,
    type PartialUdpClientConfig
} from "../index.js"


export type AnyPartialClientConfig =
    PartialTcpClientConfig
    | PartialUdpClientConfig


export type RetClient<TConfig extends AnyPartialClientConfig> =
    TConfig["socketType"] extends "udp" ? UdpClient
    : TConfig["socketType"] extends "tcp-ipcs" ? TcpClient
    : Client


export function makeClient<
    TConfig extends AnyPartialClientConfig
>(
    config: TConfig
): RetClient<TConfig>
{
    switch (config.socketType)
    {
        case "tcp-ipcs":
            return new TcpClient(
                config as PartialTcpClientConfig
            ) as RetClient<TConfig>
        case "udp":
            return new UdpClient(
                config as PartialUdpClientConfig
            ) as RetClient<TConfig>
    }
}
