import {
    Client,
    type ClientConfig,
    type ClientReceiveOpts,
    SocketState,
    type PartialClientConfig,
    type PartialClientReceiveOpts,
    type SocketStatus,
    DEFAULT_CLIENT_ADDRESS,
    DEFAULT_PORT,
    ReceivedMessage,
    RemoteInfo,
    Data,
    DEFAULT_AUTO_RECONNECT_TIMEOUT
} from "../index.js"
import type {Buffer} from "node:buffer"
import net from "node:net"


export type SpecificTcpClientConfig =
    net.NetConnectOpts
    & {
        socketType: "tcp-ipcs"
    }


export type TcpClientConfig =
    ClientConfig
    & SpecificTcpClientConfig


export type PartialTcpClientConfig =
    Partial<SpecificTcpClientConfig>
    & PartialClientConfig


export class TcpClient
    extends Client<TcpClientConfig>
{
    private _socket: net.Socket | null
    private readonly _state: SocketState
    private _status: SocketStatus

    /**
     * Constructor.
     * Defaults:
     * - address: DEFAULT_CLIENT_ADDRESS
     * - autoReconnectTimeout: DEFAULT_AUTO_RECONNECT_TIMEOUT
     * - port: DEFAULT_PORT
     * - socketType: "tcp-ipcs"
     * @param config - The configuration.
     */
    public constructor(config?: PartialTcpClientConfig)
    {
        const fullConfig: TcpClientConfig = {
            "address": DEFAULT_CLIENT_ADDRESS,
            "autoReconnectTimeout": DEFAULT_AUTO_RECONNECT_TIMEOUT,
            "port": DEFAULT_PORT,
            "socketType": "tcp-ipcs",
            ...config,
        }
        super(fullConfig)
        this._socket = null
        this._state = new SocketState()
        this._status = "closed"
    }

    public override close(): Promise<void>
    {
        const self = this
        if (self._status === "closed") return Promise.resolve()
        if (self._status === "closing")
        {
            return new Promise((
                resolve: () => void,
                reject: (err: Error) => void
            ) => {
                self._state.setClose(resolve, reject)
            })
        }

        self._status = "closing"

        try
        {
            self._socket?.destroy()
        }
        catch
        {}

        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) => {
            self._state.setClose(resolve, reject)
        })
    }

    public override open(): Promise<void>
    {
        const self = this
        switch (self._status)
        {
            case "open":
                return Promise.resolve()
            case "closing":
                return Promise.reject(new Error(self._status))
            case "closed":
                self._status = "opening"
                return new Promise((
                    resolve: () => void,
                    reject: (err: Error) => void
                ) =>
                {
                    self._state.setOpen(
                        (): void => {
                            self._status = "open"
                            resolve()
                        },
                        (): void => {
                            const status = self._status
                            self._status = "closed"
                            reject(new Error(status))
                        })

                    if (self._socket !== null)
                    {
                        self._status = "closed"
                        self._state.reject(new Error("Internal bug"))
                        return
                    }

                    self._socket = net.createConnection(self._config)
                    self._socket.on(
                        "close",
                        () => {self._onClose()}
                    )
                    self._socket.on(
                        "connect",
                        () => {self._onConnect()}
                    )
                    //socket.on("end", () => {}) // emitted when other side closes --> close
                    //socket.on("error", (err) => {}) // --> close
                    //socket.once("timeout", () => {}) // must close connection
                    self._socket.on(
                        "error",
                        (err) => {self._onError(err)}
                    )
                    self._socket.on(
                        "data",
                        (msg) => {
                            const rinfo: RemoteInfo = {
                                "address": self._socket?.remoteAddress ?? "",
                                "family": (self._socket?.remoteFamily as
                                    "IPv4" | "IPv6" | undefined) ?? "IPv4",
                                "port": self._socket?.remotePort ?? 0,
                                "size": msg.byteLength
                            }
                            self._onMessage(msg, rinfo)
                        }
                    )
                    self._socket.connect(
                        self._config.port,
                        self._config.address
                    )
                })
            case "opening":
                return new Promise((
                    resolve: () => void,
                    reject: (err: Error) => void
                ) =>
                {
                    self._state.setOpen(resolve, reject)
                })
        }
    }

    public override receive(
        opts?: PartialClientReceiveOpts
    ): Promise<ReceivedMessage>
    {
        const self = this
        const fullOpts: ClientReceiveOpts = {
            "timeoutMillis": 0,
            ...opts
        }
        if (self._status !== "open")
        {
            return Promise.reject(new Error(self._status))
        }

        return new Promise((
            resolve: (buf: ReceivedMessage) => void,
            reject: (err: Error) => void
        ) =>
        {
            self._state.setReceive(resolve, reject, fullOpts.timeoutMillis)
        })
    }

    public override send(msg: Data): Promise<void>
    {
        const self = this
        if (self._status !== "open")
        {
            return Promise.reject(new Error(self._status))
        }
        return new Promise((
            resolve: () => void,
            reject: (err: Error) => void
        ) =>
        {
            if (self._socket === null)
            {
                reject(new Error("closed"))
                return
            }
            self._state.setSend(resolve, reject)
            self._socket.write(msg, (err: Error | undefined): void =>
            {
                if (err)
                {
                    self._state.reject(err)
                    return
                }
                self._state.resolveVoid()
            })
        })
    }

    public override async sendReceive(
        msg: Data,
        opts?: PartialClientReceiveOpts
    ): Promise<ReceivedMessage>
    {
        const self = this
        return self.send(msg)
            .then(async(): Promise<ReceivedMessage> => {return self.receive(opts)})
    }

    public override setReceiveCallback(
        cb: ((data: ReceivedMessage) => Promise<void>) | null
    ): void
    {
        this._state.setResolveCallback(cb)
    }

    private _onClose(): void
    {
        const self = this

        const origStatus = self._status
        self._status = "closed"

        self._socket?.removeAllListeners()
        self._socket = null
        self._state.close()

        self._manageReconnection(origStatus)
    }

    private _onConnect(): void
    {
        const self = this
        self._state.resolveVoid()
    }

    private _onError(err: Error): void
    {
        const self = this
        self._state.reject(err)
    }

    private _onMessage(msg: Buffer, rinfo: RemoteInfo): void
    {
        const self = this
        self._state.resolveValue({
            "address": rinfo.address,
            "buffer": msg,
            "port": rinfo.port
        })
    }

    private _manageReconnection(status: SocketStatus): void
    {
        const self = this
        switch (status)
        {
            case "closed":
            case "closing":
                return
            case "open":
            case "opening":
                if (self._config.autoReconnectTimeout >= 0)
                {
                    setTimeout(
                        () => {
                            self.open().catch(async() => {return Promise.resolve()})
                        },
                        self._config.autoReconnectTimeout
                    )
                }
        }
    }
}
