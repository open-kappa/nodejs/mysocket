import type {
    Data,
    ReceivedMessage,
    SocketType
} from "../index.js"


export type ClientConfig = {
    address: string
    autoReconnectTimeout: number
    port: number
    socketType: SocketType
}


export type PartialClientConfig =
    {socketType: SocketType}
    & Partial<ClientConfig>


export type ClientReceiveOpts = {
    timeoutMillis: number
}


export type PartialClientReceiveOpts = Partial<ClientReceiveOpts>


export abstract class Client<
    TConfig extends ClientConfig = ClientConfig
>
{
    protected readonly _config: TConfig

    /**
     * Constructor.
     * @param config - The socket configuration.
     */
    public constructor(config: TConfig)
    {
        this._config = config
    }

    /**
     * Close the socket.
     * If already closed, do nothing.
     * @returns A completion promise.
     */
    public abstract close(): Promise<void>

    /**
     * Open the socket.
     * If already open, do nothing.
     * If connection ongoing, wait connection.
     * @returns A completion promise.
     */
    public abstract open(): Promise<void>

    /**
     * Receive a message.
     * If the socket is closed, fail.
     * If the socket is closed during receiving, could fail.
     * If a receiv is already in progress, enqueue.
     * Option defaults:
     * - opts.timeoutMillis: the maximum awaiting timeout. Zero or less, or not
     *   given, means no timeout.
     * @param opts - The options.
     * @returns The received data.
     */
    public abstract receive(
        opts?: PartialClientReceiveOpts
    ): Promise<ReceivedMessage>

    /**
     * Send a message.
     * If the socket is closed, fail.
     * If the socket is closed during sending, could fail.
     * If a send is already in progress, enqueue.
     * @param msg - The message.
     * @returns A completion promise.
     */
    public abstract send(msg: Data): Promise<void>

    /**
     * Send a message and receive the response.
     * Combine a `send()` and a `receive()`.
     * @param msg - The message.
     * @param opts - The options.
     * @returns The received data.
     */
    public abstract sendReceive(
        msg: Data,
        opts?: PartialClientReceiveOpts
    ): Promise<ReceivedMessage>

    /**
     * Set a callback to receive data asynchronously.
     * The callback is always called, even if a `receive()` or `sendReceive()`
     * is called.
     */
    public abstract setReceiveCallback(
        cb: ((data: ReceivedMessage) => Promise<void>) | null
    ): void
}
