import type {ReceivedMessage} from "../index.js"


type _Reject = (err: Error) => void
type _RejectPromise = (err: Error) => Promise<void>
type _Resolve = (buf: ReceivedMessage) => void
type _ResolvePromise = (buf: ReceivedMessage) => Promise<void>
type _ResolveVoid = () => void


/**
 * Internal socket state of callbacks and promises.
 */
export class SocketState
{
    private _callbackReject: _RejectPromise | null
    private _callbackResolve: _ResolvePromise | null

    private _closeReject: _Reject | null
    private _closeResolve: _ResolveVoid | null

    private _openReject: _Reject | null
    private _openResolve: _ResolveVoid | null

    private _openTimeoutId: NodeJS.Timeout | null

    private _receiveReject: _Reject | null
    private _receiveResolve: _Resolve | null

    private _receiveTimeoutId: NodeJS.Timeout | null

    private _sendReject: _Reject | null
    private _sendResolve: _ResolveVoid | null

    /**
     * Constructor.
     */
    public constructor()
    {
        this._callbackReject = null
        this._callbackResolve = null
        this._closeReject = null
        this._closeResolve = null
        this._openReject = null
        this._openResolve = null
        this._openTimeoutId = null
        this._receiveReject = null
        this._receiveResolve = null
        this._receiveTimeoutId = null
        this._sendReject = null
        this._sendResolve = null
    }


    /**
     * Clear the state.
     * Preserve user callbacks.
     */
    public clear(): void
    {
        const self = this
        //
        // self._callbackReject = null
        // self._callbackResolve = null
        self._closeReject = null
        self._closeResolve = null
        self._openReject = null
        self._openResolve = null
        self._openTimeoutId = null
        self._receiveReject = null
        self._receiveResolve = null
        self._receiveTimeoutId = null
        self._sendReject = null
        self._sendResolve = null
    }

    /**
     * Clone the state.
     * @returns The state copy.
     */
    public clone(): SocketState
    {
        const self = this
        const ret = new SocketState()
        ret._callbackReject = self._callbackReject
        ret._callbackResolve = self._callbackResolve
        ret._closeReject = self._closeReject
        ret._closeResolve = self._closeResolve
        ret._openReject = self._openReject
        ret._openResolve = self._openResolve
        ret._openTimeoutId = self._openTimeoutId
        ret._receiveReject = self._receiveReject
        ret._receiveResolve = self._receiveResolve
        ret._receiveTimeoutId = self._receiveTimeoutId
        ret._sendReject = self._sendReject
        ret._sendResolve = self._sendResolve
        return ret
    }

    /**
     * Call resolve close, rejecting pending.
     */
    public close(): void
    {
        const self = this
        const copy = self.clone()
        self.clear()

        if (copy._openTimeoutId !== null)
        {
            clearTimeout(copy._openTimeoutId)
            copy._openTimeoutId = null
        }

        if (copy._receiveTimeoutId !== null)
        {
            clearTimeout(copy._receiveTimeoutId)
            copy._receiveTimeoutId = null
        }

        const err = new Error("closed")

        copy._closeResolve?.()
        copy._openReject?.(err)

        copy._callbackReject?.(err).catch(async() => {return Promise.resolve()})
        copy._receiveReject?.(err)
        copy._sendReject?.(err)
    }

    /**
     * Call resolve open, rejecting pending.
     */
    public open(): void
    {
        const self = this
        const copy = self.clone()
        self.clear()

        if (copy._openTimeoutId !== null)
        {
            clearTimeout(copy._openTimeoutId)
            copy._openTimeoutId = null
        }

        if (copy._receiveTimeoutId !== null)
        {
            clearTimeout(copy._receiveTimeoutId)
            copy._receiveTimeoutId = null
        }

        const err = new Error("open")

        copy._openResolve?.()
        copy._closeReject?.(err)

        copy._callbackReject?.(err).catch(async() => {return Promise.resolve()})
        copy._receiveReject?.(err)
        copy._sendReject?.(err)
    }

    /**
     * Call all reject methods.
     * @param err - The error to reject.
     */
    public reject(err: Error): void
    {
        const self = this
        const copy = self.clone()
        self.clear()

        if (copy._openTimeoutId !== null)
        {
            clearTimeout(copy._openTimeoutId)
            copy._openTimeoutId = null
        }

        if (copy._receiveTimeoutId !== null)
        {
            clearTimeout(copy._receiveTimeoutId)
            copy._receiveTimeoutId = null
        }

        copy._callbackReject?.(err).catch(async() => {return Promise.resolve()})
        copy._closeReject?.(err)
        copy._openReject?.(err)
        copy._receiveReject?.(err)
        copy._sendReject?.(err)
    }

    /**
     * Call all resolve methods with a parameter.
     * @param msg - The value to resolve with.
     */
    public resolveValue(msg: ReceivedMessage): void
    {
        const self = this
        const copy = self.clone()
        self.clear()

        if (copy._openTimeoutId !== null)
        {
            clearTimeout(copy._openTimeoutId)
            copy._openTimeoutId = null
        }

        if (copy._receiveTimeoutId !== null)
        {
            clearTimeout(copy._receiveTimeoutId)
            copy._receiveTimeoutId = null
        }

        copy._callbackResolve?.(msg)
            .catch(async() => {return Promise.resolve()})
        //copy._closeResolve?.()
        //copy._openResolve?.()
        copy._receiveResolve?.(msg)
        //copy._sendResolve?.()
    }

    /**
     * Call all resolve void methods.
     */
    public resolveVoid(): void
    {
        const self = this
        const copy = self.clone()
        self.clear()

        if (copy._openTimeoutId !== null)
        {
            clearTimeout(copy._openTimeoutId)
            copy._openTimeoutId = null
        }

        if (copy._receiveTimeoutId !== null)
        {
            clearTimeout(copy._receiveTimeoutId)
            copy._receiveTimeoutId = null
        }

        //copy._callbackResolve?.(buf)
        //    .catch(async() => {return Promise.resolve()})
        copy._closeResolve?.()
        copy._openResolve?.()
        //copy._receiveResolve?.(buf)
        copy._sendResolve?.()
    }

    /**
     * Set user reject callback.
     * @param reject - The reject callback.
     */
    public setRejectCallback(
        reject: _RejectPromise | null
    ): void
    {
        const self = this
        self._callbackReject = reject
    }

    /**
     * Set user resolve callbacks.
     * @param resolve - The resolve callback.
     */
    public setResolveCallback(
        resolve: _ResolvePromise | null,
    ): void
    {
        const self = this
        self._callbackResolve = resolve
    }

    /**
     * Set close callbacks by enqueuing.
     * @param resolve - The resolve callback.
     * @param reject - The reject callback.
     */
    public setClose(resolve: _ResolveVoid, reject: _Reject): void
    {
        const self = this
        if (self._closeReject === null || self._closeResolve === null)
        {
            self._closeReject = reject
            self._closeResolve = resolve
            return
        }

        const originalRes = self._closeResolve
        const originalRej = self._closeReject
        self._closeReject = (err: Error): void =>
        {
            originalRej(err)
            reject(err)
        }
        self._closeResolve = (): void =>
        {
            originalRes()
            resolve()
        }
    }

    /**
     * Set open callbacks by enqueuing.
     * @param resolve - The resolve callback.
     * @param reject - The reject callback.
     */
    public setOpen(resolve: _ResolveVoid, reject: _Reject): void
    {
        const self = this
        if (self._openReject === null || self._openResolve === null)
        {
            self._openReject = reject
            self._openResolve = resolve
            return
        }

        const originalRes = self._openResolve
        const originalRej = self._openReject
        self._openReject = (err: Error): void =>
        {
            originalRej(err)
            reject(err)
        }
        self._openResolve = (): void =>
        {
            originalRes()
            resolve()
        }
    }

    /**
     * Set receive callbacks by postponing.
     * @param resolve - The resolve callback.
     * @param reject - The reject callback.
     * @param timeoutMillis - The timeout in millis.
     */
    public setReceive(
        resolve: _Resolve,
        reject: _Reject,
        timeoutMillis: number
    ): void
    {
        const self = this
        if (self._receiveReject === null || self._receiveResolve === null)
        {
            if (timeoutMillis > 0)
            {
                self._receiveTimeoutId = setTimeout(() => {
                    reject(new Error("timeout"))
                }, timeoutMillis)
            }
            self._receiveReject = reject
            self._receiveResolve = resolve
            return
        }

        const originalRes = self._receiveResolve
        const originalRej = self._receiveReject
        self._receiveReject = (err: Error): void =>
        {
            originalRej(err)
            if (timeoutMillis > 0)
            {
                self._receiveTimeoutId = setTimeout(() => {
                    reject(new Error("timeout"))
                }, timeoutMillis)
            }
            self._receiveReject = reject
            self._receiveResolve = resolve
        }
        self._receiveResolve = (msg: ReceivedMessage): void =>
        {
            originalRes(msg)
            if (timeoutMillis > 0)
            {
                self._receiveTimeoutId = setTimeout(() => {
                    reject(new Error("timeout"))
                }, timeoutMillis)
            }
            self._receiveReject = reject
            self._receiveResolve = resolve
        }
    }

    /**
     * Set send callbacks by postponing.
     * @param resolve - The resolve callback.
     * @param reject - The reject callback.
     */
    public setSend(resolve: _ResolveVoid, reject: _Reject): void
    {
        const self = this
        if (self._sendReject === null || self._sendResolve === null)
        {
            self._sendReject = reject
            self._sendResolve = resolve
            return
        }

        const originalRes = self._sendResolve
        const originalRej = self._sendReject
        self._sendReject = (err: Error): void =>
        {
            originalRej(err)
            self._sendReject = reject
            self._sendResolve = resolve
        }
        self._sendResolve = (): void =>
        {
            originalRes()
            self._sendReject = reject
            self._sendResolve = resolve
        }
    }
}
