import type dgram from "node:dgram"

export type RemoteInfo = dgram.RemoteInfo
