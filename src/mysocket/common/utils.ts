
export async function forAll<T>(
    foo: (...args: Array<any>) => Promise<T>,
    ...args: Array<Parameters<typeof foo>>
): Promise<T extends void ? void : Array<T>>
{
    const proms: Array<Promise<T>> = []
    for (const arg of args)
    {
        proms.push(foo(...arg))
    }
    const res = await Promise.all(proms)
    return res as (T extends void ? void : Array<T>)
}
