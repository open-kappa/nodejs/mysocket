import type {Data} from "../index.js";
import type {Buffer} from "node:buffer"


export type ReceivedMessage = {
    address: string
    buffer: Buffer
    port: number
}


export type SentMessage = {
    data: Data
}
