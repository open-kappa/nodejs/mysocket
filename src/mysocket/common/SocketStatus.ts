export type SocketStatus = "closed" | "closing" | "open" | "opening"
