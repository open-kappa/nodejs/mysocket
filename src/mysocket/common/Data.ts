import {Buffer} from "node:buffer"


export type Data = Buffer | string


export function dataToBuffer(data: Data): Buffer
{
    if (typeof data === "string") return Buffer.from(data, "utf-8")
    return data
}
