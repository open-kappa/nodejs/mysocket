# @open-kappa/mysocket

This package provides a library to simplify test management.
It supports also *node-red* and *typescript*.

## Links

* Project homepage: [hosted on GitLab Pages](
  https://open-kappa.gitlab.io/nodejs/mysocket)
* Project sources: [hosted on gitlab.com](
  https://gitlab.com/open-kappa/nodejs/mysocket)
* List of open-kappa projects: [hosted on GitLab Pages](
  https://open-kappa.gitlab.io)

## License

*@open-kappa/mysocket* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

## Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
