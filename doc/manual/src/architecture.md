# ARCHITECTURE

The library makes a clear distiction whether a socket is used to perform
client-side or server-side connections.

The `ServerSocket` class provides the interface for a server connection, and by
default reacts to incoming requests.

The `ClientSocket` class provides the interface for a client connection, and by
default initiates the communication.
