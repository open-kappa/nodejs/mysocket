# MOTIVATIONS

This simple and small library aims to simplify writing code which needs the use
of sockets in Node.JS. It prefers using a uniform interface, regardless of the
actual socket type, and exploits promises.

Please open issues on [gitlab.com](
https://gitlab.com/open-kappa/nodejs/mysocket/issues) to propose new features
and pull requests.
