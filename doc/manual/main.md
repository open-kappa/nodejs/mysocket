![MySocket logo](mysocket.png)

This package provides a library to simplify socket management.
It is written in *typescript*.

# Links

* Project homepage: [hosted on GitLab Pages](
   https://open-kappa.gitlab.io/nodejs/mysocket)
* Project sources: [hosted on gitlab.com](
   https://gitlab.com/open-kappa/nodejs/mysocket)

# License

*@open-kappa/mysocket* is released under the liberal MIT License.
Please refer to the LICENSE.txt project file for further details.

# Patrons

This node module has been sponsored by [Gizero Energie s.r.l.](
www.gizeroenergie.it).
