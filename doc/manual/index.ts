/* eslint-disable */

/**
 * @module -- Manual --
 */

export class Manual
{
    private constructor()
    {}

    /**
     * [[include:motivations.md]]
     */
    "1. Motivations": void;

    /**
     * [[include:architecture.md]]
     */
    "2. Architecture": void;

    /**
     * [[include:howto-client.md]]
     */
    "3. HOWTO Client": void;

    /**
     * [[include:howto-server.md]]
     */
    "4. HOWTO Server": void;

    /**
     * [[include:contributing.md]]
     */
    "5. Contributing": void;

    /**
     * [[include:copyright.md]]
     */
    "6. Copyright": void;

    /**
     * [[include:changelog.md]]
     */
    "7. Changelog": void;
}
